﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockingController : MonoBehaviour
{/* 
    public float speed = 20;
    public float maxSpeedChange = 3;
    public float minDistance = 5;
    public float noiseWeight = 1;
    public float alignmentWeight = 0.5f;
    public float cohesionWeight = 0.5f;


    static FlockingController manager;
    static List<FlockingController> boids = new List<FlockingController>();

    Rigidbody body;
    static Vector3 averageVelocity; // alignment
    static Vector3 averagePosition; // cohesion

    void Awake()
    {
        body = GetComponent<Rigidbody>();
        boids.Add(this);
        if (manager == null) manager = this;
    }    

    void Start()
    {
        body.velocity = Random.insideUnitSphere * speed;
    }

    void Update()
    {
        if (manager == null || !manager.gameObject.activeSelf) manager = this;

        int num = boids.Count;
        if (manager == this)
        {
            averageVelocity = Vector3.zero;
            averagePosition = Vector3.zero;
            for (int i = num-1; i >= 0; i--)
            {
                averageVelocity += boids[i].body.velocity;
                averagePosition += boids[i].transform.position;
            }
            averageVelocity /= num;
            averagePosition /= num;
        }

        Vector3 separation = Vector3.zero;
        for (int i = num-1; i >= 0; i--)
        {
            if (boids[i] == this) continue;
            Vector3 diff = transform.position - boids[i].transform.position;
            float len = diff.sqrManitude;
            float scale = Mathf.Clamp01(1 - len / minDistance);
            separation += diff.normalized * scale;
        }

        Vector3 cohesion = averagePosition - transform.position;

        Vector3 targetVelocity = cohesion * cohesionWeight + separation + averageVelocity * alignmentWeight;
        targetVelocity += new Vector3(
           Mathf.PerlinNoise(Time.time, separation.x) * 2 -1,
           Mathf.PerlinNoise(Time.time, separation.y) * 2 -1,
           Mathf.PerlinNoise(Time.time, separation.z) * 2 -1
        ) * noiseWeight;

        if (targetVelocity.sqrManitude > speed * speed)
        {
            targetVelocity = targetVelocity.normalized * speed;
        }

        Vector3 velocityChange = targetVelocity - body.velocity;
        if (velocityChange.sqrManitude > maxSpeedChange * maxSpeedChange)
        {
            velocityChange = velocityChange.normalized * maxSpeedChange;
        }

        body.AddForce(velocityChange, ForceMode.VelocityChange);

        transform.forward = targetVelocity.normalized;
    }*/
}
