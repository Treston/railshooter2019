﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    public float speed = 5;
    public Transform target;
    public Vector3 offset = new Vector3(0, 1, -10);

    void FixedUpdate()
    {
        if (target == null) return;

		Vector3 off = target.right * offset.x 
					+ target.up * offset.y 
					+ target.forward * offset.z;

        transform.position = Vector3.Lerp(
            transform.position,
            target.position + off,
            speed * Time.fixedDeltaTime
        );

		transform.rotation = Quaternion.Slerp(
			transform.rotation,
			target.rotation,
			speed * Time.deltaTime
		);
    }

	void Update()
	{
		if (!Application.isPlaying)
		{
			transform.position = target.position + offset;
		}
	}
}
