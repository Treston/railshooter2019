﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingBuildingScript : MonoBehaviour
{
    public static FallingBuildingScript instance;

   Animator anim;
   Rigidbody body;

      void Awake()
    {
        if (instance == null) instance = this;
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<PlayerController>())
        {
            anim.SetTrigger("Push");
        }
        
    }

}
