﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    public float damage = 10;
    public bool deactiveOnHit = false;

    private void OnTriggerEnter(Collider c)
    {
        OnHit(c.gameObject);
    }

    private void OnCollisionEnter(Collision c)
    {
        OnHit(c.gameObject); 
    }


    private void OnHit(GameObject g)
    {
        ShieldController s = g.GetComponentInChildren<ShieldController>();
        if (s != null) return;

        HealthController h = g.GetComponentInParent<HealthController>();
        if (h != null)
        {
            h.ChangeHealth(-damage);
        }

        if (deactiveOnHit)
        {
            gameObject.SetActive(false);
        }
    }
}
