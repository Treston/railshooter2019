﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public static TextController instance;
    public GameObject textbox;
    Text text;

    Queue<IEnumerator> textQueue = new Queue<IEnumerator>();

    void Awake()
    {
        if (instance == this) instance = this;

        text = textbox.GetComponentInChildren<Text>();
        
    }    

    IEnumerator Start()
    {
        textbox.SetActive(false);
        while (enabled)
        {
            if (textQueue.Count > 0)
            {
                textbox.SetActive(true);
                while(textQueue.Count > 0)
                {
                    yield return StartCoroutine(textQueue.Dequeue());
                }
                textbox.SetActive(false);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public static void DisplayTextAndWait (string text, float wait)
    {
        instance.textQueue.Enqueue(instance.DisplayTextAndWaitCoroutine(text, wait));
    }

    IEnumerator DisplayTextAndWaitCoroutine (string text, float wait)
    {
        this.text.text = text;
        yield return new WaitForSeconds(wait);
    }

    public static void RevealText(string text, float delay)
    {
        instance.textQueue.Enqueue(instance.RevealTextCoroutine(text, delay));
    }

    IEnumerator RevealTextCoroutine(string text, float delay
        )
    {
        this.text.text = "";
        for (int i = 0; i < text.Length; i++)
        {
            this.text.text = text.Substring(0, i);
            yield return new WaitForSeconds(delay);
        }
        yield return new WaitForSeconds(1);
    }
}
