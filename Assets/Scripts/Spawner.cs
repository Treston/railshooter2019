﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static Spawner spawner;
    public int poolSize = 100;
    public GameObject[] prefabs;
    private GameObject[][] pools;

    void Awake()
    {
       if (spawner == null)
       {
            spawner = this;
            DontDestroyOnLoad(gameObject);
       }
       else
       {
            Debug.LogWarning("Spawner already exists. Disabling.");
            gameObject.SetActive(false);
       }
    }

    void Start()
    {
        pools = new GameObject[prefabs.Length][];
        for (int i = 0; i < prefabs.Length; i++)
        {
            pools[i] = new GameObject[poolSize];
            for (int j = 0; j < poolSize; j++)
            {
                GameObject g = Instantiate(prefabs[i]);
                g.SetActive(false);
                pools[i][j] = g;
            }
        }
    }

    public static GameObject Spawn (string name)
    {
        for (int i = 0; i < spawner.prefabs.Length; i++)
        {
            if (spawner.prefabs[i].name == name)
            {
                for (int j = 0; j < spawner.poolSize; j++)
                {
                    if (!spawner.pools[i][j].activeSelf)
                    {
                        return spawner.pools[i][j];
                    }
                }
                break;
            }
        }
        return null;
    }

    private static bool IsInactive (GameObject g)
    {
        return !g.activeSelf;
    }
}
