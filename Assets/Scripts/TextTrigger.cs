﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTrigger : MonoBehaviour
{
    public string displayText = "";

    private void OnTriggerEnter(Collider c)
    {
        if (c.GetComponentInParent<PlayerController>() == null) return;

        TextController.RevealText(displayText, 0.1f);
    }
}
