﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolController : MonoBehaviour
{
    //   *****IMPORTANT***** Helps for sound and for particals
    public GameObject prefab;
    public int poolSize = 10;

    //int index = 0;
    private GameObject[] pool;

    void Start()
    {
        pool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            pool[i] = Instantiate(prefab);
        }
        //For GameObjects
        // GameObject g = Instantiate(prefab);
        //g.SetActive(false);
        //pool.Add(g);
    }

    public GameObject Spawn()
    {
        for (int i = 0; i < poolSize; i++)
        {
            if (!pool[i].activeSelf) return pool[i];
        }
        return null;

        //For GameObjects
        //GameObject g = pool.Find(IsInactive);
        //if(g == null)
        //{
        //    g = Instantiate(prefab);
        //    pool.Add(g);
        //}
        //g.SetActive(true);
        //return g;
    }

    bool IsInactive (GameObject g)
    {
        return !g.activeSelf;
    }
}
