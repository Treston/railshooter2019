﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{
    public Gradient grad;
    Image image;
    HealthController health;

    public Text gameOverText;

    void Start ()
    {
        image = GetComponent<Image>();
        health = PlayerController.instance.GetComponent<HealthController>();
        health.onHealthChanged += UpdateFill;
        health.onHealthChanged += UpdateColor;
        gameOverText.gameObject.SetActive(false);
    }

    void OnDisable ()
    {
        health.onHealthChanged -= UpdateFill;
        health.onHealthChanged += UpdateColor;
    }

    void UpdateFill (float health, float maxHealth)
    {
        image.fillAmount = health / maxHealth;
    }

    void UpdateColor (float health, float maxHealth)
    {
        image.color = grad.Evaluate(health / maxHealth);
    }

}
