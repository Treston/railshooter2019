﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public enum State {
       Idle,
       Fire,
       Reload,
   }
   public State state = State.Idle;
   public float speed = 10;
   public float activacteDist = 200;
   public bool canFire = false;

   Rigidbody body;
   Animator anim;
   void Awake ()
   {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
   } 

   void Update () {
       switch (state){
            case State.Idle:
                IdleUpdate();
                break;
            case State.Fire:
                FireUpdate();
                break;
            case State.Reload:
                ReloadUpdate();
                break;
            default:
                Debug.LogWarning("Illegal state.");
                state = State.Idle;
                break;
       }
   }

    void IdleUpdate () 
    {
        Vector3 diff = PlayerController.instance.transform.position - transform.position;
        if (diff.sqrMagnitude < activacteDist * activacteDist){
            if (canFire){
                state = State.Fire;
                anim.Play("Fire");
            }
            else {
                state = State.Reload;
                anim.Play("Reload");
            }
        }
    }

    void FireUpdate () 
    {
        body.velocity = transform.forward * speed;
    }

    void ReloadUpdate () 
    {
        body.velocity = transform.forward * speed;
    }

}
