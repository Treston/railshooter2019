﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishLineController : MonoBehaviour
{
    public Text greatJobText;

    private void Start()
    {
        greatJobText.gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<PlayerController>())
        {
            greatJobText.gameObject.SetActive(true);
            StartCoroutine(BackToMenu());
        }

    }

    IEnumerator BackToMenu()
    {
        SceneManager.LoadScene("Menu");
        yield return new WaitForSeconds(10);
    }
}
