﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthController : MonoBehaviour
{
    public delegate void HealthChanged(float health, float maxHealth);
    public event HealthChanged onHealthChanged = delegate { };

    public Text gameOverText;

    public float maxHealth = 100;

    private float health;

    void Start()
    {
        health = maxHealth;
        onHealthChanged(health, maxHealth);
    }

    public void ChangeHealth(float change)
    {
        health += change;
        onHealthChanged(health, maxHealth);

        if (health <= 0)
        {
            gameObject.SetActive(false);
            gameOverText.gameObject.SetActive(true);
			SceneManager.LoadScene("Menu");
        }
    }

}
