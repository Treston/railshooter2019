﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingCountController : MonoBehaviour
{
    Text text;
    int count = 0;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        RingController.onHitRing += UpdateCount;
    }

    private void OnDisable()
    {
        RingController.onHitRing -= UpdateCount;
    }

    void UpdateCount (bool hitRing)
    {
        if (hitRing) count++;
        else count = 0;

        text.text = count.ToString("000");
    }
}
