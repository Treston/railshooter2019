﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public static EnemyController enemy;
    
    public float minDist = 50;
    public float delay = 3;
    public float fireDelay = 0.5f;
    public float secondsAhead = 0.05f;
    GunController gun;
    
    void Awake()
    {
        gun = GetComponentInChildren<GunController>();
    }

    IEnumerator Start ()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(Random.value * delay);
        while (enabled)
        {
            Vector3 diff = PlayerController.instance.transform.position - gun.transform.position;
            diff += PlayerController.instance.GetComponent<Rigidbody>().velocity * secondsAhead;
            float d = diff.sqrMagnitude;
            if (d < minDist * minDist)
            {
                gun.transform.forward = diff;
                gun.Fire();
                yield return new WaitForSeconds(fireDelay);
            }
            else
            {
                yield return new WaitForSeconds(delay);
            }
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<BulletController>())
        {
            Die();
        }
    }

    public static void Die()
    {
        enemy.gameObject.SetActive(false);
    }

}
