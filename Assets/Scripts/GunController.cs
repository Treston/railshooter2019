﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public string bulletName = "PlayerBullet";

    Rigidbody body;

    private void Awake()
    {
        body = GetComponentInParent<Rigidbody>();
    }

    public void Fire()
    {
        GameObject g = Spawner.Spawn(bulletName);
        g.transform.position = transform.position;
        g.transform.rotation = transform.rotation;
        g.SetActive(true);
        BulletController b = g.GetComponent<BulletController>();

        if (body == null)
        {
            b.Fire(transform.forward, Vector3.zero);
        }

        else
        {
            b.Fire(transform.forward, body.velocity);
        }
    }
}
