﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour
{
    public delegate void HitRing(bool hit);
    public static event HitRing onHitRing = delegate { };

    public float radius = 2;

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<PlayerController>())
        {
            bool didHit = (transform.position - c.transform.position).sqrMagnitude < radius * radius;
            onHitRing(didHit);
		    AudioManager.instance.Play("RingSound");
        } 
    }

    void OnTriggerExit(Collider c)
    {
        gameObject.SetActive(false);
    }
}
