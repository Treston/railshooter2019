﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed = 30;
    public float lifetime = 2;

    BulletController bullet;
    Rigidbody body;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    public void Fire (Vector3 dir, Vector3 parentVelocity)
    {
        dir.Normalize();
        body.velocity = dir * speed + parentVelocity;
        StartCoroutine(SelfDestruct());
    }

    IEnumerator SelfDestruct ()
    {
        yield return new WaitForSeconds(lifetime);
        gameObject.SetActive(false);
    }

}
