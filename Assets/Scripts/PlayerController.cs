﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    public Vector3 speed = new Vector3(5, 5, 15);
    public float maxSpeedChange = 1;
    public float barrelRollWindow = 0.5f;
	public float rotateSpeed = 60;

    Rigidbody body;
    Animator anim;
    GunController gun;

	bool isAllRangeMode = false;

    void Awake()
    {
        if (instance == null) instance = this;
        gun = GetComponentInChildren<GunController>();
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    Vector2 animDir;
    float lastHardLeft = -1;
    float lastHardRight = -1;

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        bool turnHardLeft = Input.GetKey(KeyCode.LeftShift);
        bool turnHardRight = Input.GetKey(KeyCode.RightShift);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (Time.time - lastHardLeft < barrelRollWindow)
            {
                anim.SetTrigger("spinLeft");
            }
            lastHardLeft = Time.time;
            lastHardRight = -1;
        }

        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            if (Time.time - lastHardRight < barrelRollWindow)
            {
                anim.SetTrigger("spinRight");
            }
            lastHardRight = Time.time;
            lastHardLeft = -1;
        }

        if ((turnHardLeft && x < 0) || (turnHardRight && x > 0)) x *= 6;
        animDir = Vector2.Lerp(animDir, new Vector2(x, y), maxSpeedChange * Time.deltaTime);

		if (isAllRangeMode)
		{
			transform.Rotate(Vector3.up * x * Time.deltaTime * rotateSpeed);
		}

        Vector3 targetVelocity = transform.right * x * speed.x + Vector3.up * -y * speed.y + transform.forward * speed.z;
        Vector3 velocityChange = targetVelocity - body.velocity;
        Vector2 xyChange = (Vector2)velocityChange;

        if (xyChange.sqrMagnitude > maxSpeedChange * maxSpeedChange)
        {
            xyChange = xyChange.normalized * maxSpeedChange;
            velocityChange.x = xyChange.x;
            velocityChange.y = xyChange.y;
        }
        body.AddForce(velocityChange, ForceMode.VelocityChange);

        anim.SetFloat("x", animDir.x);
        anim.SetFloat("y", animDir.y);

        if (Input.GetButtonDown("Jump"))
        {
            gun.Fire();
			AudioManager.instance.Play("ShootingSound");
        }

		void OnTriggerEnter (Collider c)
		{
			if (c.gameObject.CompareTag("AllRange"))
			{
				isAllRangeMode = true;
			} 
		}

        bool reset = Input.GetKeyDown(KeyCode.Escape);

        if (reset == true)
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
