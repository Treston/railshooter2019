﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour
{
    void OnTriggerEnter(Collider c)
    {
        BulletController b = c.GetComponent<BulletController>();
        if (b == null) return;

        Rigidbody body = c.GetComponent<Rigidbody>();
        body.velocity = -body.velocity;
    }
}
