﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableWallController : MonoBehaviour
{
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<BulletController>())
        {
            gameObject.SetActive(false);
        }
    }
}
